package controllers

import play.api._
import play.api.mvc._
import scalaj.http._
import java.net.URLEncoder
import scala.io.Source
import java.io._
import java.io.IOException

class Application extends Controller {
  val API_KEY = "AIzaSyBGapKCbMf6TLEwyRdUt6B66kH9Os8sFgE"
  val CLIENT  = "Scala_Spam_Tester"
  val LOOKUP_URL_BASE = "https://sb-ssl.google.com/safebrowsing/api/lookup?client=" + 
  			CLIENT + "&key=" + API_KEY +"&appver=1.5.2&pver=3.1"
  
  def hello(firstname: String, secondname: String) = Action {
    Ok("Hello " + firstname + " " + secondname + "!")
  }

  def index = Action {
  	var title = "Hello"

	var badURLs:List[String] = List()
	var urlPool:List[String] = List()

	var br  : BufferedReader = new BufferedReader(new FileReader(new File("urls.txt")));
	var url : String = br.readLine()
	val startTime = System.currentTimeMillis();

	try{
	 	while (url != null){
	        val line = url.drop(1)
	        urlPool = line :: urlPool

	        if(urlPool.size == 500){
	        	isDangerous(urlPool) match{
	        		case Left(bool) => None
	        		case Right(list) => badURLs = list ::: badURLs
	        	}
	        	urlPool = List[String]()
	        }
	        url = br.readLine()
		}
		isDangerous(urlPool) match{
    		case Left(bool) => None
    		case Right(list) => badURLs = list ::: badURLs
    	}
	}catch{
		case ioe: IOException => ioe.printStackTrace()
		case e: Exception => e.printStackTrace()
    }

	var endTime: Long = System.currentTimeMillis()
	val difference: Long = endTime - startTime

    Ok(views.html.index(title, badURLs, difference))
  }

  def isDangerous(urlBulk : List[String]) : Either[Boolean, List[String]]= {
	var data = urlBulk.size + "\n" + urlBulk.mkString("\n")
	val result: HttpResponse[String] = Http(LOOKUP_URL_BASE).postData(data).header("content-type", "text/plain").asString

  	if(result.code == 200){
  		val errorsomeURL: Array[String] = result.body.split("[\\r\\n]+")
  		var badURLs: List[String] = List()

  		for(i <- 0 to errorsomeURL.size - 1){
  			if(errorsomeURL(i) == "malware"){
  				badURLs = urlBulk(i) :: badURLs
  			}
  		}
  		return Right(badURLs)
  	}
  	Left(false)
  }
}
